#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#include <ctype.h>
#include "Header.h"

static int brojPitanja = 0;
static PITANJE* poljePitanja = NULL;
static int counter = 0;

void pocetak(void) {

	system("cls");

	HIGHSCORE igrac;
	int polaPola = 1, zovi = 1, publika = 1;
	int r1 = 1, count = 0, granica = 0, var, j;
	char znak;

	printf("\n\n\n\n\t\t\tUnesite svoje ime: ");
	scanf("%s", igrac.ime);
	//system("pause");
	system("cls");


	FILE* hp = NULL;
	hp = fopen("highscore.bin", "ab+");
	if (hp == NULL) {
		printf("\nDatoteka se ne moze otvoriti.\n");
		perror("Otvaranje");
	}

	poljePitanja = (PITANJE*)ucitavanjePitanja();
	if (poljePitanja == NULL) {
		perror("Zauzimanje memorije za sortirana pitanja");
		exit(EXIT_FAILURE);
	}

	PITANJE vraceno;
	char tocno;

	do {
		switch (r1) {
		case 1:
			printf("\n\t********** Pitanje za 100 **********");
			vraceno = sortiranjePolja(poljePitanja, 1);
			tocno = ispisivanjePitanja(vraceno);
			provjeraJokera(publika, polaPola, zovi);
			printf("\n\tDa bi ste odustali odaberite \"O\"");
			j = 1;
			do {
				printf("\n\tUnesite vas odgovor: ");
				znak = getch();
				if (znak == 'j') {
					printf("\n\tUnesite zeljeni joker: ");
					scanf("%d", &var);
					if (var == 1) {
						jokerPublika(tocno, publika);
						publika = -1;
						j = 1;
					}
					if (var == 2) {
						jokerPolaPola(vraceno, polaPola, tocno);
						polaPola = -1;
						j = 1;
					}
					if (var == 3) {
						jokerZovi(tocno, zovi);
						zovi = -1;
						j = 1;
					}
				}
				else if (znak == tocno)
				{
					printf("\n\n\t\tTocno!!!");
					count = 100;
					r1++;
					j = 0;
					printf("\n\t");
					system("pause");
					system("cls");
					break;
				}
				else if (znak == 'o') {
					printf("\n\tOdustali ste\n");
					count = 0;
					r1 = 0;
					printf("\n\t");
					system("pause");
					system("cls");
					break;
				}
				else
				{
					printf("\n\n\tNetocno!!! Tocan odgovor je pod %c:%s", toupper(tocno), vraceno.odg1);
					count = 0;
					j = 0;
					r1 = 0;
					printf("\n\t");
					system("pause");
					system("cls");
					break;
				}
			} while (j);
			break;

		case 2:
			printf("\n\t********** Pitanje za 200 **********");
			vraceno = sortiranjePolja(poljePitanja, 2);
			tocno = ispisivanjePitanja(vraceno);
			provjeraJokera(publika, polaPola, zovi);
			printf("\n\tDa bi ste odustali odaberite \"O\"");
			j = 1;
			do {
				printf("\n\tUnesite vas odgovor: ");
				znak = getch();
				if (znak == 'j') {
					printf("\n\tUnesite zeljeni joker: ");
					scanf("%d", &var);
					if (var == 1) {
						jokerPublika(tocno, publika);
						publika = -1;
						j = 1;
					}
					if (var == 2) {
						jokerPolaPola(vraceno, polaPola, tocno);
						polaPola = -1;
						j = 1;
					}
					if (var == 3) {
						jokerZovi(tocno, zovi);
						zovi = -1;
						j = 1;
					}
				}
				else if (znak == tocno)
				{
					printf("\n\n\t\tTocno!!!");
					count = 200;
					r1++;
					j = 0;
					printf("\n\t");
					system("pause");
					system("cls");
					break;
				}
				else if (znak == 'o') {
					printf("\n\tOdustali ste\n");
					count = 100;
					r1 = 0;
					printf("\n\t");
					system("pause");
					system("cls");
					break;
				}
				else
				{
					printf("\n\n\tNetocno!!! Tocan odgovor je pod %c:%s", toupper(tocno), vraceno.odg1);
					count = 0;
					j = 0;
					r1 = 0;
					printf("\n\t");
					system("pause");
					system("cls");
					break;
				}
			} while (j);
			break;

		case 3:
			printf("\n\t********** Pitanje za 300 **********");
			vraceno = sortiranjePolja(poljePitanja, 3);
			tocno = ispisivanjePitanja(vraceno);
			provjeraJokera(publika, polaPola, zovi);
			printf("\n\tDa bi ste odustali odaberite \"O\"");
			j = 1;
			do {
				printf("\n\tUnesite vas odgovor: ");
				znak = getch();
				if (znak == 'j') {
					printf("\n\tUnesite zeljeni joker: ");
					scanf("%d", &var);
					if (var == 1) {
						jokerPublika(tocno, publika);
						publika = -1;
						j = 1;
					}
					if (var == 2) {
						jokerPolaPola(vraceno, polaPola, tocno);
						polaPola = -1;
						j = 1;
					}
					if (var == 3) {
						jokerZovi(tocno, zovi);
						zovi = -1;
						j = 1;
					}
				}
				else if (znak == tocno)
				{
					printf("\n\n\t\tTocno!!!");
					count = 300;
					r1++;
					j = 0;
					printf("\n\t");
					system("pause");
					system("cls");
					break;
				}
				else if (znak == 'o') {
					printf("\n\tOdustali ste\n");
					count = 200;
					r1 = 0;
					printf("\n\t");
					system("pause");
					system("cls");
					break;
				}
				else
				{
					printf("\n\n\tNetocno!!! Tocan odgovor je pod %c:%s", toupper(tocno), vraceno.odg1);
					count = 0;
					j = 0;
					r1 = 0;
					printf("\n\t");
					system("pause");
					system("cls");
					break;
				}
			} while (j);
			break;

		case 4:
			printf("\n\t********** Pitanje za 500 **********");
			vraceno = sortiranjePolja(poljePitanja, 4);
			tocno = ispisivanjePitanja(vraceno);
			provjeraJokera(publika, polaPola, zovi);
			printf("\n\tDa bi ste odustali odaberite \"O\"");
			j = 1;
			do {
				printf("\n\tUnesite vas odgovor: ");
				znak = getch();
				if (znak == 'j') {
					printf("\n\tUnesite zeljeni joker: ");
					scanf("%d", &var);
					if (var == 1) {
						jokerPublika(tocno, publika);
						publika = -1;
						j = 1;
					}
					if (var == 2) {
						jokerPolaPola(vraceno, polaPola, tocno);
						polaPola = -1;
						j = 1;
					}
					if (var == 3) {
						jokerZovi(tocno, zovi);
						zovi = -1;
						j = 1;
					}
				}
				else if (znak == tocno)
				{
					printf("\n\n\t\tTocno!!!");
					count = 500;
					r1++;
					j = 0;
					printf("\n\t");
					system("pause");
					system("cls");
					break;
				}
				else if (znak == 'o') {
					printf("\n\tOdustali ste\n");
					count = 500;
					r1 = 0;
					printf("\n\t");
					system("pause");
					system("cls");
					break;
				}
				else
				{
					printf("\n\n\tNetocno!!! Tocan odgovor je pod %c:%s", toupper(tocno), vraceno.odg1);
					count = 0;
					j = 0;
					r1 = 0;
					printf("\n\t");
					system("pause");
					system("cls");
					break;
				}
			} while (j);
			break;

		case 5:
			printf("\n\t********** Pitanje za 1000 **********");
			vraceno = sortiranjePolja(poljePitanja, 5);
			tocno = ispisivanjePitanja(vraceno);
			provjeraJokera(publika, polaPola, zovi);
			printf("\n\tDa bi ste odustali odaberite \"O\"");
			do {
				printf("\n\tUnesite vas odgovor: ");
				znak = getch();
				if (znak == 'j') {
					printf("\n\tUnesite zeljeni joker: ");
					scanf("%d", &var);
					if (var == 1) {
						jokerPublika(tocno, publika);
						publika = -1;
						j = 1;
					}
					if (var == 2) {
						jokerPolaPola(vraceno, polaPola, tocno);
						polaPola = -1;
						j = 1;
					}
					if (var == 3) {
						jokerZovi(tocno, zovi);
						zovi = -1;
						j = 1;
					}
				}
				else if (znak == tocno)
				{
					printf("\n\n\t\tTocno!!!");
					count = 1000;
					r1++;
					j = 0;
					printf("\n\t");
					system("pause");
					system("cls");
					break;
				}
				else if (znak == 'o') {
					printf("\n\tOdustali ste\n");
					count = 1000;
					r1 = 0;
					printf("\n\t");
					system("pause");
					system("cls");
					break;
				}
				else
				{
					printf("\n\n\tNetocno!!! Tocan odgovor je pod %c:%s", toupper(tocno), vraceno.odg1);
					count = 0;
					j = 0;
					r1 = 0;
					printf("\n\t");
					system("pause");
					system("cls");
					break;
				}
			} while (j);
			break;

		case 6:
			printf("\n\t********** Pitanje za 2000 **********");
			vraceno = sortiranjePolja(poljePitanja, 6);
			tocno = ispisivanjePitanja(vraceno);
			provjeraJokera(publika, polaPola, zovi);
			printf("\n\tDa bi ste odustali odaberite \"O\"");
			j = 1;
			do {
				printf("\n\tUnesite vas odgovor: ");
				znak = getch();
				if (znak == 'j') {
					printf("\n\tUnesite zeljeni joker: ");
					scanf("%d", &var);
					if (var == 1) {
						jokerPublika(tocno, publika);
						publika = -1;
						j = 1;
					}
					if (var == 2) {
						jokerPolaPola(vraceno, polaPola, tocno);
						polaPola = -1;
						j = 1;
					}
					if (var == 3) {
						jokerZovi(tocno, zovi);
						zovi = -1;
						j = 1;
					}
				}
				else if (znak == tocno)
				{
					printf("\n\n\t\tTocno!!!");
					count = 2000;
					granica = 1000;
					r1++;
					j = 0;
					printf("\n\t");
					system("pause");
					system("cls");
					break;
				}
				else if (znak == 'o') {
					printf("\n\tOdustali ste\n");
					count = 1000;
					r1 = 0;
					printf("\n\t");
					system("pause");
					system("cls");
					break;
				}
				else
				{
					printf("\n\n\tNetocno!!! Tocan odgovor je pod %c:%s", toupper(tocno), vraceno.odg1);
					count = 0;
					granica = 1000;
					j = 0;
					r1 = 0;
					printf("\n\t");
					system("pause");
					system("cls");
					break;
				}
			} while (j);
			break;

		case 7:
			printf("\n\t********** Pitanje za 4000 **********");
			vraceno = sortiranjePolja(poljePitanja, 7);
			tocno = ispisivanjePitanja(vraceno);
			provjeraJokera(publika, polaPola, zovi);
			printf("\n\tDa bi ste odustali odaberite \"O\"");
			j = 1;
			do {
				printf("\n\tUnesite vas odgovor: ");
				znak = getch();
				if (znak == 'j') {
					printf("\n\tUnesite zeljeni joker: ");
					scanf("%d", &var);
					if (var == 1) {
						jokerPublika(tocno, publika);
						publika = -1;
						j = 1;
					}
					if (var == 2) {
						jokerPolaPola(vraceno, polaPola, tocno);
						polaPola = -1;
						j = 1;
					}
					if (var == 3) {
						jokerZovi(tocno, zovi);
						zovi = -1;
						j = 1;
					}
				}
				else if (znak == tocno)
				{
					printf("\n\n\t\tTocno!!!");
					count = 4000;
					r1++;
					j = 0;
					printf("\n\t");
					system("pause");
					system("cls");
					break;
				}
				else if (znak == 'o') {
					printf("\n\tOdustali ste\n");
					count = 2000;
					r1 = 0;
					printf("\n\t");
					system("pause");
					system("cls");
					break;
				}
				else
				{
					printf("\n\n\tNetocno!!! Tocan odgovor je pod %c:%s", toupper(tocno), vraceno.odg1);
					count = 0;
					j = 0;
					r1 = 0;
					printf("\n\t");
					system("pause");
					system("cls");
					break;
				}
			} while (j);
			break;

		case 8:
			printf("\n\t********** Pitanje za 8000 **********");
			vraceno = sortiranjePolja(poljePitanja, 8);
			tocno = ispisivanjePitanja(vraceno);
			provjeraJokera(publika, polaPola, zovi);
			printf("\n\tDa bi ste odustali odaberite \"O\"");
			j = 1;
			do {
				printf("\n\tUnesite vas odgovor: ");
				znak = getch();
				if (znak == 'j') {
					printf("\n\tUnesite zeljeni joker: ");
					scanf("%d", &var);
					if (var == 1) {
						jokerPublika(tocno, publika);
						publika = -1;
						j = 1;
					}
					if (var == 2) {
						jokerPolaPola(vraceno, polaPola, tocno);
						polaPola = -1;
						j = 1;
					}
					if (var == 3) {
						jokerZovi(tocno, zovi);
						zovi = -1;
						j = 1;
					}
				}
				else if (znak == tocno)
				{
					printf("\n\n\t\tTocno!!!");
					count = 8000;
					r1++;
					j = 0;
					printf("\n\t");
					system("pause");
					system("cls");
					break;
				}
				else if (znak == 'o') {
					printf("\n\tOdustali ste\n");
					count = 4000;
					r1 = 0;
					printf("\n\t");
					system("pause");
					system("cls");
					break;
				}
				else
				{
					printf("\n\n\tNetocno!!! Tocan odgovor je pod %c:%s", toupper(tocno), vraceno.odg1);
					count = 0;
					j = 0;
					r1 = 0;
					printf("\n\t");
					system("pause");
					system("cls");
					break;
				}
			} while (j);
			break;

		case 9:
			printf("\n\t********** Pitanje za 16 000 **********");
			vraceno = sortiranjePolja(poljePitanja, 9);
			tocno = ispisivanjePitanja(vraceno);
			provjeraJokera(publika, polaPola, zovi);
			printf("\n\tDa bi ste odustali odaberite \"O\"");
			j = 1;
			do {
				printf("\n\tUnesite vas odgovor: ");
				znak = getch();
				if (znak == 'j') {
					printf("\n\tUnesite zeljeni joker: ");
					scanf("%d", &var);
					if (var == 1) {
						jokerPublika(tocno, publika);
						publika = -1;
						j = 1;
					}
					if (var == 2) {
						jokerPolaPola(vraceno, polaPola, tocno);
						polaPola = -1;
						j = 1;
					}
					if (var == 3) {
						jokerZovi(tocno, zovi);
						zovi = -1;
						j = 1;
					}
				}
				else if (znak == tocno)
				{
					printf("\n\n\t\tTocno!!!");
					count = 16000;
					r1++;
					j = 0;
					printf("\n\t");
					system("pause");
					system("cls");
					break;
				}
				else if (znak == 'o') {
					printf("\n\tOdustali ste\n");
					count = 8000;
					r1 = 0;
					printf("\n\t");
					system("pause");
					system("cls");
					break;
				}
				else
				{
					printf("\n\n\tNetocno!!! Tocan odgovor je pod %c:%s", toupper(tocno), vraceno.odg1);
					count = 0;
					j = 0;
					r1 = 0;
					printf("\n\t");
					system("pause");
					system("cls");
					break;
				}
			} while (j);
			break;

		case 10:
			printf("\n\t********** Pitanje za 32 000 **********");
			vraceno = sortiranjePolja(poljePitanja, 10);
			tocno = ispisivanjePitanja(vraceno);
			provjeraJokera(publika, polaPola, zovi);
			printf("\n\tDa bi ste odustali odaberite \"O\"");
			j = 1;
			do {
				printf("\n\tUnesite vas odgovor: ");
				znak = getch();
				if (znak == 'j') {
					printf("\n\tUnesite zeljeni joker: ");
					scanf("%d", &var);
					if (var == 1) {
						jokerPublika(tocno, publika);
						publika = -1;
						j = 1;
					}
					if (var == 2) {
						jokerPolaPola(vraceno, polaPola, tocno);
						polaPola = -1;
						j = 1;
					}
					if (var == 3) {
						jokerZovi(tocno, zovi);
						zovi = -1;
						j = 1;
					}
				}
				else if (znak == tocno)
				{
					printf("\n\n\t\tTocno!!!");
					count = 32000;
					r1++;
					j = 0;
					printf("\n\t");
					system("pause");
					system("cls");
					break;
				}
				else if (znak == 'o') {
					printf("\n\tOdustali ste\n");
					count = 16000;
					r1 = 0;
					printf("\n\t");
					system("pause");
					system("cls");
					break;
				}
				else
				{
					printf("\n\n\tNetocno!!! Tocan odgovor je pod %c:%s", toupper(tocno), vraceno.odg1);
					count = 0;
					j = 0;
					r1 = 0;
					printf("\n\t");
					system("pause");
					system("cls");
					break;
				}
			} while (j);
			break;

		case 11:
			printf("\n\t********** Pitanje za 64 000 **********");
			vraceno = sortiranjePolja(poljePitanja, 11);
			tocno = ispisivanjePitanja(vraceno);
			provjeraJokera(publika, polaPola, zovi);
			printf("\n\tDa bi ste odustali odaberite \"O\"");
			j = 1;
			do {
				printf("\n\tUnesite vas odgovor: ");
				znak = getch();
				if (znak == 'j') {
					printf("\n\tUnesite zeljeni joker: ");
					scanf("%d", &var);
					if (var == 1) {
						jokerPublika(tocno, publika);
						publika = -1;
						j = 1;
					}
					if (var == 2) {
						jokerPolaPola(vraceno, polaPola, tocno);
						polaPola = -1;
						j = 1;
					}
					if (var == 3) {
						jokerZovi(tocno, zovi);
						zovi = -1;
						j = 1;
					}
				}
				else if (znak == tocno)
				{
					printf("\n\n\t\tTocno!!!");
					count = 64000;
					granica = 32000;
					r1++;
					j = 0;
					printf("\n\t");
					system("pause");
					system("cls");
					break;
				}
				else if (znak == 'o') {
					printf("\n\tOdustali ste\n");
					count = 32000;
					r1 = 0;
					printf("\n\t");
					system("pause");
					system("cls");
					break;
				}
				else
				{
					printf("\n\n\tNetocno!!! Tocan odgovor je pod %c:%s", toupper(tocno), vraceno.odg1);
					count = 0;
					granica = 32000;
					j = 0;
					r1 = 0;
					printf("\n\t");
					system("pause");
					system("cls");
					break;
				}
			} while (j);
			break;

		case 12:
			printf("\n\t********** Pitanje za 125 000 **********");
			vraceno = sortiranjePolja(poljePitanja, 12);
			tocno = ispisivanjePitanja(vraceno);
			provjeraJokera(publika, polaPola, zovi);
			printf("\n\tDa bi ste odustali odaberite \"O\"");
			j = 1;
			do {
				printf("\n\tUnesite vas odgovor: ");
				znak = getch();
				if (znak == 'j') {
					printf("\n\tUnesite zeljeni joker: ");
					scanf("%d", &var);
					if (var == 1) {
						jokerPublika(tocno, publika);
						publika = -1;
						j = 1;
					}
					if (var == 2) {
						jokerPolaPola(vraceno, polaPola, tocno);
						polaPola = -1;
						j = 1;
					}
					if (var == 3) {
						jokerZovi(tocno, zovi);
						zovi = -1;
						j = 1;
					}
				}
				else if (znak == tocno)
				{
					printf("\n\n\t\tTocno!!!");
					count = 125000;
					r1++;
					j = 0;
					printf("\n\t");
					system("pause");
					system("cls");
					break;
				}
				else if (znak == 'o') {
					printf("\n\tOdustali ste\n");
					count = 64000;
					r1 = 0;
					printf("\n\t");
					system("pause");
					system("cls");
					break;
				}
				else
				{
					printf("\n\n\tNetocno!!! Tocan odgovor je pod %c:%s", toupper(tocno), vraceno.odg1);
					count = 0;
					j = 0;
					r1 = 0;
					printf("\n\t");
					system("pause");
					system("cls");
					break;
				}
			} while (j);
			break;

		case 13:
			printf("\n\t********** Pitanje za 250 000 **********");
			vraceno = sortiranjePolja(poljePitanja, 13);
			tocno = ispisivanjePitanja(vraceno);
			provjeraJokera(publika, polaPola, zovi);
			printf("\n\tDa bi ste odustali odaberite \"O\"");
			j = 1;
			do {
				printf("\n\tUnesite vas odgovor: ");
				znak = getch();
				if (znak == 'j') {
					printf("\n\tUnesite zeljeni joker: ");
					scanf("%d", &var);
					if (var == 1) {
						jokerPublika(tocno, publika);
						publika = -1;
						j = 1;
					}
					if (var == 2) {
						jokerPolaPola(vraceno, polaPola, tocno);
						polaPola = -1;
						j = 1;
					}
					if (var == 3) {
						jokerZovi(tocno, zovi);
						zovi = -1;
						j = 1;
					}
				}
				else if (znak == tocno)
				{
					printf("\n\n\t\tTocno!!!");
					count = 250000;
					r1++;
					j = 0;
					printf("\n\t");
					system("pause");
					system("cls");
					break;
				}
				else if (znak == 'o') {
					printf("\n\tOdustali ste\n");
					count = 125000;
					r1 = 0;
					printf("\n\t");
					system("pause");
					system("cls");
					break;
				}
				else
				{
					printf("\n\n\tNetocno!!! Tocan odgovor je pod %c:%s", toupper(tocno), vraceno.odg1);
					count = 0;
					j = 0;
					r1 = 0;
					printf("\n\t");
					system("pause");
					system("cls");
					break;
				}
			} while (j);
			break;

		case 14:
			printf("\n\t********** Pitanje za 500 000 **********");
			vraceno = sortiranjePolja(poljePitanja, 14);
			tocno = ispisivanjePitanja(vraceno);
			provjeraJokera(publika, polaPola, zovi);
			printf("\n\tDa bi ste odustali odaberite \"O\"");
			j = 1;
			do {
				printf("\n\tUnesite vas odgovor: ");
				znak = getch();
				if (znak == 'j') {
					printf("\n\tUnesite zeljeni joker: ");
					scanf("%d", &var);
					if (var == 1) {
						jokerPublika(tocno, publika);
						publika = -1;
						j = 1;
					}
					if (var == 2) {
						jokerPolaPola(vraceno, polaPola, tocno);
						polaPola = -1;
						j = 1;
					}
					if (var == 3) {
						jokerZovi(tocno, zovi);
						zovi = -1;
						j = 1;
					}
				}
				else if (znak == tocno)
				{
					printf("\n\n\t\tTocno!!!");
					count = 500000;
					r1++;
					j = 0;
					printf("\n\t");
					system("pause");
					system("cls");
					break;
				}
				else if (znak == 'o') {
					printf("\n\tOdustali ste\n");
					count = 250000;
					r1 = 0;
					printf("\n\t");
					system("pause");
					system("cls");
					break;
				}
				else
				{
					printf("\n\n\tNetocno!!! Tocan odgovor je pod %c:%s", toupper(tocno), vraceno.odg1);
					count = 0;
					j = 0;
					r1 = 0;
					printf("\n\t");
					system("pause");
					system("cls");
					break;
				}
			} while (j);
			break;

		case 15:
			printf("\n\t********** Pitanje za 1 000 000 **********");
			vraceno = sortiranjePolja(poljePitanja, 15);
			tocno = ispisivanjePitanja(vraceno);
			provjeraJokera(publika, polaPola, zovi);
			printf("\n\tDa bi ste odustali odaberite \"O\"");
			j = 1;
			do {
				printf("\n\tUnesite vas odgovor: ");
				znak = getch();
				if (znak == 'j') {
					printf("\n\tUnesite zeljeni joker: ");
					scanf("%d", &var);
					if (var == 1) {
						jokerPublika(tocno, publika);
						publika = -1;
						j = 1;
					}
					if (var == 2) {
						jokerPolaPola(vraceno, polaPola, tocno);
						polaPola = -1;
						j = 1;
					}
					if (var == 3) {
						jokerZovi(tocno, zovi);
						zovi = -1;
						j = 1;
					}
				}
				else if (znak == tocno)
				{
					printf("\n\n\t\tTOCNO!!!\n\t\tOSVOJILI STE MILIJUN KUNA!!!");
					count = 1000000;
					r1 = 0;
					j = 0;
					printf("\n\t");
					system("pause");
					system("cls");
					break;
				}
				else if (znak == 'o') {
					printf("\n\tOdustali ste\n");
					count = 500000;
					r1 = 0;
					printf("\n\t");
					system("pause");
					system("cls");
					break;
				}
				else
				{
					printf("\n\n\tNetocno!!! Tocan odgovor je pod %c:%s", toupper(tocno), vraceno.odg1);
					count = 0;
					j = 0;
					r1 = 0;
					printf("\n\t");
					system("pause");
					system("cls");
					break;
				}
			} while (j);
			break;
		}
	} while (r1);

	igrac.highscore = (count >= granica ? count : granica);
	system("cls");
	printf("\n\tVase ime:%s i vas highscore: %d\n\t", igrac.ime, igrac.highscore);

	fprintf(hp, "%8d %s", igrac.highscore, igrac.ime);


	fclose(hp);
	//free(poljePitanja);
	//printf("\n\t");
	int uvijet;
	while ((uvijet = getchar()) != '\n' && uvijet != EOF) {}
	system("pause");
	system("cls");
}

void* ucitavanjePitanja(void) {
	FILE* pF = fopen("pitanja.bin", "rb");
	if (pF == NULL) {
		perror("Ucitavanje pitanja iz datoteke pitanja.bin");
		return NULL;
	}

	fread(&brojPitanja, sizeof(int), 1, pF);
	//printf("\nbroj pitanja: %d\n", brojPitanja);

	PITANJE* poljePitanja = { NULL };
	poljePitanja = (PITANJE*)calloc(brojPitanja, sizeof(PITANJE));
	if (poljePitanja == NULL) {
		perror("Zauzimanje memorije za pitanja");
		return NULL;
	}

	fread(poljePitanja, sizeof(PITANJE), brojPitanja, pF);
	fclose(pF);
	return poljePitanja;
}

PITANJE sortiranjePolja(PITANJE* poljePitanja, int tezina) {

	PITANJE* sortirano = NULL;
	int i;
	counter = 0;
	for (i = 0; i < brojPitanja; i++) {
		if ((poljePitanja + i)->tezina == tezina) {
			counter++;
		}
	}
	//printf("\nBroj pitanja za ovu razinu %d", counter);
	sortirano = (PITANJE*)calloc(counter, sizeof(PITANJE));
	if (sortirano == NULL) {
		perror("Zauzimanje memorije za sortirana pitanja");
		exit(EXIT_FAILURE);
	}

	int j = 0;
	for (i = 0; i < brojPitanja; i++) {
		if ((poljePitanja + i)->tezina == tezina) {
			sortirano[j] = poljePitanja[i];
			j++;
		}
	}

	int p[3], y;
	PITANJE odabrano;

	srand((unsigned)time(NULL));

	for (i = 0; i < 3; i++) {
		p[i] = (float)rand() / RAND_MAX * counter;
		//("\n%d", p[i]);
	}
	y = p[2];
	odabrano = sortirano[y];
	//printf("\ny je jednako: %d a conter je jednak %d", y, counter);

	return odabrano;
}

char ispisivanjePitanja(PITANJE odabrano) {


	int x;
	printf("\n\n\t%s", odabrano.pitanje);
	x = (float)rand() / RAND_MAX * 1000;
	//printf("\novo je x: %d", x);

	char tocni;

	if (0 <= x && x < 250) {
		printf("\n\tA:%s\tB:%s\tC:%s\tD:%s", odabrano.odg2, odabrano.odg3, odabrano.odg1, odabrano.odg4);
		tocni = 'c';
	}
	if (250 <= x && x < 500) {
		printf("\n\tA:%s\tB:%s\tC:%s\tD:%s", odabrano.odg4, odabrano.odg1, odabrano.odg2, odabrano.odg3);
		tocni = 'b';
	}
	if (500 <= x && x < 750) {
		printf("\n\tA:%s\tB:%s\tC:%s\tD:%s", odabrano.odg3, odabrano.odg2, odabrano.odg4, odabrano.odg1);
		tocni = 'd';
	}
	if (750 <= x && x <= 1000) {
		printf("\n\tA:%s\tB:%s\tC:%s\tD:%s", odabrano.odg1, odabrano.odg4, odabrano.odg3, odabrano.odg2);
		tocni = 'a';
	}
	return tocni;
}


void pomoc(void) {


	system("cls");
	printf("\n *********************************** Dobrodosli u kviz \"TKO ZELI BITI MILIJUNAS ?\" **********************************");
	printf("\n **********************************************************************************************************************");
	printf("\n\n\t>>Pravila igre <<");

	printf("\n\n\tTKO ZELI BITI MILIJUNAS? (u izvorniku: Who Wants to Be a Millionaire? ) natjecateljski\
\n\tje televizijski format kviza znanja u kojem natjecatelj odgovara na pitanja opceg znanja\
\n\tnastojeci, uz pomoc tri dzokera, uspjesno odgovoriti na 15 pitanja od kojih svako\
\n\tima cetiri ponudena odgovora, a natjecatelj se mora odluciti za jedan odgovor koji smatra tocnim\n");

	printf("\n\tU slucaju da natjecatelj tocno odgovori na pojedino postavljeno pitanje ima pravo\
\n\tdgovarati na sljedece pitanje. Vrijednost osvojenog iznosa uvecava se za svaki sljedeci tocan odgovor.\
\n\tTocnim odgovorom na posljednje, 15. pitanje, osvaja maksimalnu nagradu od 1.000.000,00 kuna.");

	printf("\n\tTijekom odgovaranja postoje dva praga: prvi je prag na 5. pitanju, koje u slucaju\
\n\ttocnog odgovora natjecatelju donosi 1.000 kuna; a drugi je na 10. pitanju koje u slucaju\
\n\ttocnog odgovora natjecatelju donosi 32.000 kuna. Kad natjecatelj tijekom odgovaranja prvi put ponudi\
\n\tnetocan odgovor, eliminiran je iz daljnjeg natjecanja, ali nije svejedno u kojem je trenutku pogrijesio.");

	printf("\n\n\t>> Opcija \"ODUSTANI\" <<");
	printf("\n\n\tNatjecatelj moze u bilo kojem trenutku prekunuti s igrim ukoiko to zeli.\
\n\tSve sto je potrebno jest kliknuti \"O\" na tipkovnici i time automatski prekida s igrom\
\n\ti time ostvaruje rezultat zadnjeg tocno odgovorenog pitanja");

	printf("\n\n\t>>Opis dzokera \"POLA-POLA\" (50:50) <<");
	printf("\n\n\tAko natjecatelj zeli iskoristiti dzoker 'pola-pola' dva ce se odgovora od cetiri\
\n\tponudena odgovora na pitanje izbrisati i ostaviti natjecatelju izbor izmedu\
\n\tsamo dva odgovora: jednog tocnog i jednog netocnog. Joker se aktivira pritisom tipke \"J\".");

	printf("\n\n\t>> Opis dzokera \"PITAJ PUBLIKU\" <<");
	printf("\n\n\tSvatko od publike u studiju ima uredaj za glasovanje s cetiri tipke obiljezene slovima\
\n\ta, b, c, i d te ce glasati po zelji. Natjecatelju ce biti prikazani dobiveni postotci za svaki\
\n\tpojedini odgovor, a natjecatelj ce nakon toga sam odluciti koji od ponudena cetiri odgovora smatra tocnim.\
\n\tJoker se aktivira pritisom tipke \"J\".");

	printf("\n\n\t>> Opis dzokera \"ZOVI\" <<");
	printf("\n\n\tNatjecatelj ima mogucost 'nazvati' u pomoc nekog od svojih kontakata. Na ekranu ce biti\
\n\tprikazan odgovor za koji nazvani kontakt smatra da je tocan, a natjecatelj ima pravo na to hoce li ili nece\
\n\tuzeti u obzir pomoc pri dososenju konacnog odgovora. Joker se aktivira pritisom tipke \"J\".");

	printf("\n\n\n\tPritisnite enter za povratak na pocetni zaslon!\n");
	system("pause");
	system("cls");
}

char izlazIzPrograma(void) {

	printf("Zelite li uistinu zatvoriti program?\n");
	printf("Utipkajte 'da' ako zelite zatvoriti program\n");
	char potvrda[3] = { '\0' };
	scanf("%2s", potvrda);
	if (!strcmp("da", potvrda)) {
		return NULL;
	}

	int c;
	while ((c = getchar()) != '\n' && c != EOF) {}
	system("pause");
	system("cls");
	return '1';
}

void admin(void) {

	system("cls");
	//kopirano sa interneta
	char username[15];
	char password[12];


	printf("Unesite korisnicko ime: ");
	scanf("%s", &username);

	printf("Unesite zaporku: ");
	scanf("%s", &password);

	if (strcmp(username, "maliPas") == 0) {
		if (strcmp(password, "123") == 0) {
			printf("Uspjesno ste se ulogirali");
			system("cls");
			dodajPitanje();
		}
		else {
			printf("\nPogeresna lozinka");
		}
	}
	else {
		printf("\nNepostojano korisnicko ime");
	}

	printf("\nPritisnite enter za povratak na pocetni zaslon!\n");
	system("pause");
	system("cls");

}

void kreiranjeDatoteke(void) {

	FILE* fp = NULL;
	fp = fopen("pitanja.bin", "rb");
	if (fp == NULL) {

		fp = fopen("pitanja.bin", "wb");
		if (fp == NULL) {
			perror("Kreiranje datoteke pitanja.bin");
			exit(EXIT_FAILURE);
		}
		fwrite(&brojPitanja, sizeof(int), 1, fp);
		fclose(fp);
	}
	else {
		fwrite(&brojPitanja, sizeof(int), 1, fp);
		fclose(fp);
	}

}

void dodajPitanje(void) {

	kreiranjeDatoteke();
	FILE* pF = fopen("pitanja.bin", "rb+");
	if (pF == NULL) {
		perror("Dodavanje studenta u datoteke pitanja.bin");
		exit(EXIT_FAILURE);
	}
	fread(&brojPitanja, sizeof(int), 1, pF);
	printf("Ukupan broj pitanja: %d\n", brojPitanja);

	PITANJE temp = { 0 };
	int t, c;

	printf("\nUnesite tezinu pitanja od 1 do 15: ");
	do {
		scanf("%d", &t);
		if (1 > t || t > 15) {
			printf("\nUnjeli ste nedozvoljen broj! Uneiste neki od brojeva u trazenom intervalu: ");
			while ((c = getchar()) != '\n' && c != EOF) {}
		}
	} while (!(1 <= t && t <= 15));
	temp.tezina = t;
	getchar();

	printf("Unesite pitanje!\n");
	fgets(temp.pitanje, 199, stdin);

	printf("Unesite odgovor!\n");
	fgets(temp.odg1, 49, stdin);

	printf("Unesite odgovor!\n");
	fgets(temp.odg2, 49, stdin);

	printf("Unesite odgovor!\n");
	fgets(temp.odg3, 49, stdin);

	printf("Unesite odgovor!\n");
	fgets(temp.odg4, 49, stdin);

	fseek(pF, sizeof(PITANJE) * brojPitanja, SEEK_CUR);
	fwrite(&temp, sizeof(PITANJE), 1, pF);
	rewind(pF);
	brojPitanja++;
	fwrite(&brojPitanja, sizeof(int), 1, pF);
	fclose(pF);
	printf("\n\t");
	system("pause");
	system("cls");

}


void highscore(void) {


	FILE* hp = NULL;
	hp = fopen("highscore.bin", "rb");
	if (hp == NULL) {
		printf("\nDatoteka se ne moze otvoriti.\n");
		perror("Otvaranje");
	}


	HIGHSCORE* hs;
	int highscore;
	char ime[20];
	int i = 0, brojac = 0;


	while ((fscanf(hp, "%7d %s", &highscore, ime)) != EOF) {
		brojac++;
	}

	hs = (HIGHSCORE*)calloc(brojac, sizeof(HIGHSCORE));

	rewind(hp);

	while ((fscanf(hp, "%7d %s", &(hs + i)->highscore, (hs + i)->ime)) != EOF) {
		i++;
	}

	selectionSort(hs, brojac);

	fclose(hp);

	//printf("\nPritisnite enter za povratak na pocetni zaslon!\n");
	printf("\n\t");
	system("pause");
	system("cls");
}



void selectionSort(HIGHSCORE* hs, int n) {


	HIGHSCORE temp;

	int min = -1, i, j;
	for (i = 0; i < n - 1; i++) {
		min = i;
		for (j = i + 1; j < n; j++) {
			if ((hs + j)->highscore > (hs + min)->highscore) {
				min = j;
			}
		}

		temp = hs[min];
		hs[min] = hs[i];
		hs[i] = temp;
	}

	j = 1;
	//printf("\nOvo je n:%d", n);
	if (n >= 10) {
		for (i = 0; i < 10; i++) {
			printf("\n\t%d. Ime: %s\t\tHighscore: %d", j, (hs + i)->ime, (hs + i)->highscore);
			j++;
		}
	}
	else if (n < 10) {
		for (i = n - 1; i >= 0; i--) {
			printf("\n\t%d. Ime: %s\t\tHighscore: %d", j, (hs + i)->ime, (hs + i)->highscore);
			j++;
		}
	}
}

void provjeraJokera(int publika, int polaPola, int zovi) {
	printf("\n\n\n\tDa bi ste odabrali jokera pritisnite \"J\"");
	printf("\n\tOd jokera su dostupni: ");
	if (publika > 0) {
		printf("\n\tPITAJ PUBLIKU(1) : ");
	}
	if (polaPola > 0) {
		printf("POLA:POLA(2) :  ");
	}
	if (zovi > 0) {
		printf("ZOVI(3)");
	}
	else {
		printf("\n\tNemate dostupnih jokera\n");
	}
}

void jokerPublika(char odg, int provjera) {
	srand((unsigned)time(NULL));
	int g[4], i, max = 0;

	for (i = 0; i < 4; i++) {
		g[i] = (float)rand() / RAND_MAX * 100;
		if (max < g[i]) {
			max = g[i];
		}
	}

	if (provjera > 0) {
		if (odg == 'a') {
			printf("\n\tNajveci postotak je dobio odgovor A: %d%%", max);
		}
		if (odg == 'b') {
			printf("\n\tNajveci postotak je dobio odgovor B: %d%%", max);
		}
		if (odg == 'c') {
			printf("\n\tNajveci postotak je dobio odgovor C: %d%%", max);
		}
		if (odg == 'd') {
			printf("\n\tNajveci postotak je dobio odgovor D: %d%%", max);
		}
	}
	else if (provjera < 0) {
		printf("\n\tVec ste iskoristili taj joker!\n");
		printf("\n\tIli ponovno odaberite joker opciju ili unesite svoj konacan odgovor.");
	}
}

void jokerPolaPola(PITANJE odabrano, int provjera, char odg) {

	int x;
	x = (float)rand() / RAND_MAX * 1000;

	if (provjera > 0) {
		if (odg == 'a') {
			if (0 <= x && x < 335) {
				printf("\n\tA:%s\tB:%s", odabrano.odg1, odabrano.odg4);
			}
			if (335 <= x && x < 665) {
				printf("\n\tA:%s\tC:%s", odabrano.odg1, odabrano.odg3);
			}
			if (665 <= x && x < 1000) {
				printf("\n\tA:%s\tD:%s", odabrano.odg1, odabrano.odg2);
			}
		}

		if (odg == 'b') {
			if (0 <= x && x < 335) {
				printf("\n\tA:%s\tB:%s", odabrano.odg4, odabrano.odg1);
			}
			if (335 <= x && x < 665) {
				printf("\n\tB:%s\tC:%s", odabrano.odg1, odabrano.odg2);
			}
			if (665 <= x && x < 1000) {
				printf("\n\tB:%s\tD:%s", odabrano.odg1, odabrano.odg3);
			}
		}

		if (odg == 'c') {
			if (0 <= x && x < 335) {
				printf("\n\tA:%s\tC:%s", odabrano.odg2, odabrano.odg1);
			}
			if (335 <= x && x < 665) {
				printf("\n\tB:%s\tC:%s", odabrano.odg3, odabrano.odg1);
			}
			if (665 <= x && x < 1000) {
				printf("\n\tC:%s\tD:%s", odabrano.odg1, odabrano.odg4);
			}
		}

		if (odg == 'd') {
			if (0 <= x && x < 335) {
				printf("\n\tA:%s\tD:%s", odabrano.odg3, odabrano.odg1);
			}
			if (335 <= x && x < 665) {
				printf("\n\tB:%s\tD:%s", odabrano.odg2, odabrano.odg1);
			}
			if (665 <= x && x < 1000) {
				printf("\n\tC:%s\tD:%s", odabrano.odg4, odabrano.odg1);
			}
		}

		printf("\n\tUnesite svoj konacan odgovor: ");
	}
	else if (provjera < 0) {
		printf("\n\tVec ste iskoristili taj joker!\n");
		printf("\n\tIli ponovno odaberite joker opciju ili unesite svoj konacan odgovor.");
	}
}

void jokerZovi(char odg, int provjera) {
	if (provjera > 0) {
		printf("\n\tPoziv kaze %c tocan odgovor.", toupper(odg));
	}
	else if (provjera < 0) {
		printf("\n\tVec ste iskoristili taj joker!\n");
		printf("\n\tIli ponovno odaberite joker opciju ili unesite svoj konacan odgovor.");
	}
}
void pretrazivanjeHighscora(void) {

	FILE* hp = NULL;
	hp = fopen("highscore.bin", "rb");
	if (hp == NULL) {
		printf("\nDatoteka se ne moze otvoriti.\n");
		perror("Otvaranje");
	}

	HIGHSCORE* hs;
	int highscore;
	char ime[20];
	int i = 0, brojac = 0;


	while ((fscanf(hp, "%d %s", &highscore, ime)) != EOF) {
		brojac++;
	}

	hs = (HIGHSCORE*)calloc(brojac, sizeof(HIGHSCORE));

	rewind(hp);

	while ((fscanf(hp, "%d %s", &(hs + i)->highscore, (hs + i)->ime)) != EOF) {
		i++;
	}


	char s[20];
	char* string = &s;
	int f = 0;


	printf("\n\tUnesite ime trazenog igraca: ");
	fgets(string, 19, stdin);
	provjeraStringa(string);

	for (i = 0; i < brojac; i++) {
		//printf("\n\tOvo je ime iz stirnga: %s, a ovo iz polja: %s", string, (hs + i)->ime);

		if (strcmp((hs + i)->ime, string) == 0) {
			printf("\n\tIgrac je pronaden!\n");
			printf("\n\tIme:%s\t\tHighscore:%d", (hs + i)->ime, (hs + i)->highscore);
			f = 1;
			//printf("\n\t");
			//system("pause");
			//system("cls");
		}

		else if (i == (brojac - 1) && f < 1) {
			printf("\n\tTakav igrac ne postoji");
			//printf("\n\t");
			//system("pause");
			//system("cls");

		}
	}
	/*printf("\n\tTakav igrac ne postoji");
	printf("\n\t");
	system("pause");
	system("cls");*/

	fclose(hp);
	printf("\n\t");
	system("pause");
	system("cls");

}

void provjeraStringa(char* polje) {
	int n = strlen(polje);
	if (polje[n - 1] == '\n') {
		polje[n - 1] = '\0';
	}
}

void pregledPitanja(void) {

	poljePitanja = (PITANJE*)ucitavanjePitanja();


	int tezina, i;
	printf("\n\tUneiste tezinu pitanja koja zelite pretrazit: ");
	do {
		scanf("%d", &tezina);
		if (tezina < 0 || tezina > 15) {
			printf("\n\tUnjeli ste pogresnu tezinu pitanja! Unesite neku od dozvoljenih vrijednost: ");
		}
	} while (!(0 < tezina && tezina < 16));

	PITANJE* sortirano = NULL;

	counter = 0;
	for (i = 0; i < brojPitanja; i++) {
		if ((poljePitanja + i)->tezina == tezina) {
			counter++;
		}
	}
	printf("\nBroj pitanja za ovu razinu %d", counter);
	sortirano = (PITANJE*)calloc(counter, sizeof(PITANJE));
	if (sortirano == NULL) {
		perror("Zauzimanje memorije za sortirana pitanja");
		exit(EXIT_FAILURE);
	}

	int j = 0;
	for (i = 0; i < brojPitanja; i++) {
		if ((poljePitanja + i)->tezina == tezina) {
			sortirano[j] = poljePitanja[i];
			printf("\n\t%s", (sortirano + j)->pitanje);
			printf("\n\tA:%s\tB:%s\tC:%s\tD:%s", (sortirano + j)->odg1, (sortirano + j)->odg2, (sortirano + j)->odg3, (sortirano + j)->odg4);
			j++;
		}
	}
	system("pause");
	system("cls");
	int uvijet;
	while ((uvijet = getchar()) != '\n' && uvijet != EOF) {}
}

