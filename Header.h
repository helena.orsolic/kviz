#ifndef HEADER_H
#define HEADER_H

typedef struct pitanje {
	int tezina;
	char pitanje[200];
	char odg1[50];
	char odg2[50];
	char odg3[50];
	char odg4[50];
}PITANJE;

typedef struct highscore {
	char ime[20];
	long int highscore;
}HIGHSCORE;

char izbornik(void);
void pocetak(void);
void* ucitavanjePitanja(void);
PITANJE sortiranjePolja(PITANJE*, int);
char ispisivanjePitanja(PITANJE);
void pomoc(void);
char izlazIzPrograma(void);
void admin(void);
void kreiranjeDatoteke(void);
void dodajPitanje(void);
void highscore(void);
void selectionSort(HIGHSCORE*, int);
void provjeraJokera(int, int, int);
void jokerPublika(char, int);
void jokerPolaPola(PITANJE, int, char);
void jokerZovi(char, int);
void pretrazivanjeHighscora(void);
void provjeraStringa(char*);
void pregledPitanja(void);





#endif 



