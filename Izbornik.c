#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include "Header.h"

char izbornik(void) {

	printf("\n\t\t*********************************************");

	printf("\n\t\t\t    D O B R O D O S L I\n ");
	printf("\n\t\t\t             U\n ");
	printf("\n\t\t\t          K V I Z ");
	printf("\n\n\t\t\t  TKO ZELI BITI MILIJUNAS?");
	printf("\n\t\t");
	printf("\n\t\t*********************************************");
	printf("\n\t\t  OSVOJITE 1.000.000,00 kuna SVOJIM ZNANJEM");
	printf("\n\t\t*********************************************");
	printf("\n\t\t > Opcija 1: zapocnite igru");
	printf("\n\t\t > Opcija 2: pogledajte rang ljestvicu");
	printf("\n\t\t > Opcija 3: pretrazivaje igraca na rang ljestvici");
	printf("\n\t\t > Opcija 4: dodavanje pitanja");
	printf("\n\t\t > Opcija 5: pregled pitanja");
	printf("\n\t\t > Opcija 6: pomoc");
	printf("\n\t\t > Opcija 7: izlaz iz programa");
	printf("\n\t\t________________________________________\n\n");

	char uvijet;
	printf("\t\tOdaberite jednu od opcija: ");

	scanf("%c", &uvijet);
	
	getchar();

	switch (uvijet) {
	case '1':
		pocetak();
		break;
	case '2':
		highscore();
		break;
	case '3':
		pretrazivanjeHighscora();
		break;
	case '4':
		admin();
		dodajPitanje();
		break;
	case '5':
		pregledPitanja();
		break;
	case '6':
		pomoc();
		break;
	case '7':
		uvijet = izlazIzPrograma();
		break;
	default:
		printf("\n\t\tNedozvoljena radnja! Unesite neku od ponudjenih opcija\n");
		while ((uvijet = getchar()) != '\n' && uvijet != EOF) {}
		printf("\n\t\t");
		system("pause");
		system("cls");
		break;
	}
	return uvijet;
}
